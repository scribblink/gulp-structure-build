'use strict';

require('cache-require-paths');

// Require all tasks in gulp/tasks recursively
var requireDir = require('require-dir');
requireDir('./gulp/tasks', { recurse: true });